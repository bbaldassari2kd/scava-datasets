pipeline {
  agent any
  environment {
    RSYNC_PASSWD = credentials('rsync_passwd')
  }
  options {
    buildDiscarder(logRotator(numToKeepStr: '10', artifactNumToKeepStr: '5'))
  }
  stages {
    stage('Build AERI') {
      steps {
        sh '''
          wget https://castalia.camp/dl/datasets/incidents_bundles_extract.csv.bz2 -O website/content/aeri_stacktraces/incidents_bundles_extract.csv.bz2
          wget https://castalia.camp/dl/datasets/incidents_extract.csv.bz2 -O website/content/aeri_stacktraces/incidents_extract.csv.bz2
          wget https://castalia.camp/dl/datasets/incidents_full.tar.bz2 -O website/content/aeri_stacktraces/incidents_full.tar.bz2
          wget https://castalia.camp/dl/datasets/problems_extract.csv.bz2 -O website/content/aeri_stacktraces/problems_extract.csv.bz2
          wget https://castalia.camp/dl/datasets/problems_full.tar.bz2 -O website/content/aeri_stacktraces/problems_full.tar.bz2
        ''' 
      }
    }
    stage('Build mboxes') {
      steps {
        sh '''
          rm /data/eclipse_mls/*.mbox.lock || echo "No mbox lock found."
          rsync -avlp -e ssh --rsh="/usr/bin/sshpass -p ${SYS_RSYNC_PASSWD} ssh -o StrictHostKeyChecking=no -l bbaldassari2kd" build.eclipse.org:/opt/public/download-staging.priv/scava/ /data/eclipse_mls/ || echo "Rsync had issues. But the show must go on, right?"
          chmod 777 /data/eclipse_mls/*
          source ~/perl5/perlbrew/etc/bashrc 
          perlbrew switch perl-5.30.1
          perl -v 
          Rscript --version
          cd scripts/ && sh ./process_mboxes.sh
          ls /data/eclipse_mls/ > ../website/content/eclipse_mls/list_mboxes.txt
          cp /data/eclipse_mls_full.csv ../website/content/eclipse_mls/
        ''' 
      }
    }
    stage('Build projects') {
      steps {
        sh '''
          pwd
          ls
          source ~/perl5/perlbrew/etc/bashrc 
          perlbrew switch perl-5.30.1
          perl -v 
          Rscript --version
          cd scripts/ && sh ./process_all_projects.sh
        ''' 
      }
    }
    stage('Build website') {
      steps {
        sh '''
          pwd
          ls
          source ~/perl5/perlbrew/etc/bashrc 
          Rscript --version
          echo "Building website."
          cd scripts/ && sh ./build_website.sh
          echo "Cleaning website zone from non-compressed files."
          find ../website/public/ -name "*.csv" | xargs rm -rf 
          find ../website/public/ -name "git*.txt" | xargs rm -rf 
        ''' 
      }
    }
    stage('Deploy web site') {
      steps {
        sh '''
          rsync -avz website/public/ bbaldassari2kd@projects-storage.eclipse.org:/home/data/httpd/download.eclipse.org/scava/ --delete || echo "Error $?: some files/attrs were not transferred (see previous errors)"
        ''' 
      }
    }
    stage('Archiving') {
      steps {
        archiveArtifacts artifacts: 'website/public/**/*.*', fingerprint: true 
        cleanWs()
      }
    }
  }
}

