---
title: %%ECLIPSE_PROJECT_ID%%
output:
  blogdown::html_page:
    toc: true
    autoCollapseToc: false
  html_document:
    toc: yes
params:
  project_id: "%%ECLIPSE_PROJECT_ID%%"
menu:
  sidebar:
    parent: projects
---

```{r init, message=FALSE, echo=FALSE, warning=FALSE}

library(xts)
library(dygraphs)
require(xtable)
library(R.utils)

project_id <- params$project_id

file_git_commits <- "git_commits.csv"
file_git_commits_evol <- "git_commits_evol.csv"
file_git_log <- "git_log.txt"
file_bz_issues <- "bugzilla_issues.csv"
file_bz_issues_open <- "bugzilla_issues_open.csv"
file_bz_evol <- "bugzilla_evol.csv"
file_bz_components <- "bugzilla_components.csv"
file_bz_versions <- "bugzilla_versions.csv"
file_forums_posts <- "eclipse_forums_posts.csv"
file_forums_threads <- "eclipse_forums_threads.csv"
file_pmi_checks <- "eclipse_pmi_checks.csv"
file_jenkins_builds <- "jenkins_builds.csv"
file_jenkins_jobs <- "jenkins_jobs.csv"
file_sc_authors <- "scancode_authors.csv"
file_sc_copyrights <- "scancode_copyrights.csv"
file_sc_holders <- "scancode_holders.csv"
file_sc_licences <- "scancode_licences.csv"
file_sc_packages <- "scancode_packages.csv"
file_sc_pl <- "scancode_programming_languages.csv"
file_sc_sf <- "scancode_special_files.csv"
file_sq_issues_blocker <- "sq_issues_blocker.csv"
file_sq_issues_critical <- "sq_issues_critical.csv"
file_sq_issues_major <- "sq_issues_major.csv"
file_sq_metrics <- "sq_metrics.csv"
```

## About this document

This document is a [R notebook](https://rmarkdown.rstudio.com/), dynamically created from the numbers extracted on the project. It lists all datasets published for the project, providing basic numbers, figures and a quick summary, and serves as a test case to make sure that all the required data is present and roughly consistent with requirements. All plots and tables are computed from the actual data as provided in the downloads.

To re-execute the document, simply start a R session, load `rmarkdown` and `render` the page with the project ID as a parameter:

```r
require('rmarkdown')
render("datasets_report.Rmarkdown", params = list(project_id = "`r project_id`"), output_format="html_document")
```

This website uses the [blogdown](https://github.com/rstudio/blogdown) R package, which provides a different output_format for the hugo framework. 

This report was generated on ``r Sys.Date()``.


## Downloads

All data is retrieved from [Alambic](https://alambic.io), an open-source framework for development data extraction and processing. 

This project's analysis page can be found on the [Alambic instance for the Eclipse forge](https://eclipse.alambic.io), at https://eclipse.alambic.io/projects/`r project_id`.

Downloads are composed of gzip'd CSV and JSON files. CSV files always have a header to name the fields, which makes it easy to import in analysis software like R: 

```r
data <- read.csv(file='myfile.csv', header=T)
names(data)
```

List of datasets generated for the project:

`r if(file.exists(file_git_commits)){paste("
* Git
    - [Git Commits](../", file_git_commits, ".gz) (CSV) -- Full list of commits with id, message, time, author, committer, and added, deleted and modifed lines.
    - [Git Commits Evol](../", file_git_commits_evol, ".gz) (CSV) -- Evolution of number of commits and authors by day. 
    - [Git Log](../", file_git_log, ".gz) (TXT) -- the raw export of git log.
", sep='')}`
`r if(file.exists(file_bz_issues)){paste("
* Bugzilla
    - [Bugzilla issues](../", file_bz_issues, ".gz) (CSV) -- list of all issues. 
    - [Bugzilla open issues](../", file_bz_issues_open, ".gz) (CSV) -- list of issues in an open state. 
    - [Bugzilla evolution](../",file_bz_evol, ".gz) (CSV) -- weekly time series of issues created. 
    - [Bugzilla components](../", file_bz_components, ".gz) (CSV) -- list of components declared for the project. 
    - [Bugzilla versions](../", file_bz_versions, ".gz) (CSV) -- list of versions declared for the project. 
", sep='')}`
`r if(file.exists(file_forums_posts)){paste("
* Eclipse Forums
    - [Forums Posts](../", file_forums_posts, ".gz) (CSV) -- list of all forum posts for this project.
    - [Forums threads](../", file_forums_threads, ".gz) (CSV) -- list of all forum threads for this project. 
", sep='')}`
`r if(file.exists(file_jenkins_builds)){paste("
* Jenkins CI
    - [Jenkins CI Builds](../", file_jenkins_builds, ".gz) (CSV) -- list of all builds on the Jenkins CI instance. 
    - [Jenkins CI Jobs](../", file_jenkins_jobs, ".gz) (CSV) -- list of all jobs on the Jenkins CI instance. 
", sep='')}`
`r if(file.exists(file_pmi_checks)){paste("
* Eclipse PMI
    - [PMI Checks](../", file_pmi_checks, ".gz) (CSV) -- list of all checks applied to the Project Management Infrastructure entries for the project.
", sep='')}`
`r if(file.exists(file_sc_authors)){paste("
* ScanCode
    - [ScanCode files](../", file_sc_pl, ".gz) (CSV) -- list of files identified in the codebase by ScanCode. 
    - [ScanCode authors](../", file_sc_authors, ".gz) (CSV) -- list of authors identified in the codebase by ScanCode.
    - [ScanCode copyrights](../", file_sc_copyrights, ".gz) (CSV) -- list of copyrights identified in the codebase by ScanCode. 
    - [ScanCode holders](../", file_sc_holders, ".gz) (CSV) -- list of copyright holders identified in the codebase by ScanCode.
    - [ScanCode licences](../", file_sc_licences, ".gz) (CSV) -- list of licences identified in the codebase by ScanCode. 
    - [ScanCode packages](../", file_sc_packages, ".gz) (CSV) -- list of packages identified in the codebase by ScanCode. 
    - [ScanCode programming languages](../", file_sc_pl, ".gz) (CSV) -- list of programming languages identified in the codebase by ScanCode. 
", sep='')}`
`r if(file.exists(file_sq_metrics)){paste("
* SonarQube
    - [SonarQube blocker issues](../", file_sq_issues_blocker, ".gz) (CSV) -- list of blocker issues detected by SonarQube. 
    - [SonarQube critical issues](../", file_sq_issues_critical, ".gz) (CSV) -- list of critical issues detected by SonarQube.
    - [SonarQube major issues](../", file_sq_issues_major, ".gz) (CSV) -- list of major issues detected by SonarQube. 
    - [SonarQube metrics](../", file_sq_metrics, ".gz) (CSV) -- list of code metrics extracted from SonarQube. 
", sep='')}`


```{r inc_git, eval=file.exists(file_git_commits), child='datasets_report_git.inc'}
```


```{r inc_bz, eval=file.exists(file_bz_issues), child='datasets_report_bz.inc'}
```


```{r inc_forums, eval=file.exists(file_forums_posts), child='datasets_report_forums.inc'}
```


```{r inc_jenkins, eval=file.exists(file_jenkins_builds), child='datasets_report_jenkins.inc'}
```


```{r inc_pmi, eval=file.exists(file_pmi_checks), child='datasets_report_pmi.inc'}
```


```{r inc_sc, eval=file.exists(file_sc_authors), child='datasets_report_scancode.inc'}
```


```{r inc_sq, eval=file.exists(file_sq_metrics), child='datasets_report_sq.inc'}
```

