
----

## <a name="jenkins"></a> Jenkins

### <a name="jenkins_builds"></a> Builds

**Download**: [`r file_jenkins_builds`.gz](../`r file_jenkins_builds`.gz)

```{r jenkins_b_init}
data <- read.csv(file=file_jenkins_builds, header=T)
```

File is [``r file_jenkins_builds``](../`r file_jenkins_builds`.gz), and has ``r ncol(data)`` columns for ``r nrow(data)`` commits.

<table class="table table-striped">
  <tr><th>ID</th><th>Name</th><th>Time</th><th>Result</th></tr>
```{r jenkins_b_table, eval=T, echo=FALSE, message=FALSE, results='asis'}
t <- apply(head(x = data, n = 10), 1,
		   function(x) paste(' <tr><td><a href="', trimws(x[[7]]), '">', trimws(x[[4]]), '</a></td><td>', 
		                     trimws(x[[2]]), '</a></td><td>', trimws(x[[1]]), '</a></td><td>', 
		                     trimws(x[[3]]), '</td></tr>', sep=''))
t <- paste(t, collapse=" ")
cat(t)
```
</table>

```{r jenkins_b_plot, echo=FALSE, message=FALSE, results='asis'}
suppressPackageStartupMessages(library(googleVis))
options(gvis.plot.tag='chart')

data.table <- as.data.frame(table(data$result))
names(data.table) <- c('Result', 'Count')

data.table[data.table$Result == "ABORTED", c('Colour')] <- '#D3D3D3'
data.table[data.table$Result == "FAILURE", c('Colour')] <- '#DC3912'
data.table[data.table$Result == "SUCCESS", c('Colour')] <- '#109618'
data.table[data.table$Result == "UNSTABLE", c('Colour')] <- '#FF9900'

p <- gvisPieChart(data.table[,c('Result', 'Count')],
              options = list(
                title=paste("Build Results for project ", project_id, " ", sep=""),
                sliceVisibilityThreshold=0, height=280,
                colors=paste('["', paste(data.table$Colour, collapse = '", "', sep=""), '"]', collapse = '", "', sep=""),
                pieHole= 0.4))

print(p, 'chart')
```

<br />

### <a name="jenkins_jobs"></a> Jobs

**Download**: [`r file_jenkins_jobs`.gz](../`r file_jenkins_jobs`.gz)

```{r jenkins_j_init}
data <- read.csv(file=file_jenkins_jobs, header=T)
```

File is [``r file_jenkins_jobs``](../`r file_jenkins_jobs`.gz), and has ``r ncol(data)`` columns for ``r nrow(data)`` commits.

<table class="table table-striped">
  <tr><th>Name</th><th>Colour</th><th>Last build time</th><th>Health report</th></tr>
```{r jenkins_j_table, eval=T, echo=FALSE, message=FALSE, results='asis'}
t <- apply(head(x = data, n = 10), 1,
		   function(x) paste(' <tr><td><a href="', trimws(x[[15]]), '">', trimws(x[[1]]), '</a></td><td>', 
		                     trimws(x[[3]]), '</a></td><td>', trimws(x[[5]]), '</a></td><td>', 
		                     trimws(x[[14]]), '</td></tr>', sep=''))
t <- paste(t, collapse=" ")
cat(t)
```
</table>

```{r jenkins_j_plot, echo=FALSE, message=FALSE, results='asis'}
suppressPackageStartupMessages(library(googleVis))
options(gvis.plot.tag='chart')

data.table <- as.data.frame(table(data$color))
names(data.table) <- c('Result', 'Count')

data.table$Colour <- '#D3D3D3'

data.table[data.table$Result == "red", c('Colour')] <- '#DC3912'
data.table[data.table$Result == "blue", c('Colour')] <- '#109618'
data.table[data.table$Result == "yellow", c('Colour')] <- '#FF9900'

p <- gvisPieChart(data.table[,c('Result', 'Count')],
              options = list(
                title=paste("Job Results for project ", project_id, " ", sep=""),
                sliceVisibilityThreshold=0, height=280,
                colors=paste('["', paste(data.table$Colour, collapse = '", "', sep=""), '"]', collapse = '", "', sep=""),
                pieHole= 0.4))

print(p, 'chart')
```
