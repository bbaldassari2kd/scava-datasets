---
title: Eclipse Projects
linkTitle: Projects
menu:
  main:
  sidebar:
    identifier: projects
weight: 30
slug: projects
---

We generate comprehensive data extracts of a set of Eclipse projects, including data sources like:

* Software Configuration Management (Eclipse [git](https://git.eclipse.org) or [GitHub](https://github.com)),
* Issues tracking ([Bugzilla](https://bugs.eclipse.org) or [GitHub](https://github.com)),
* Project metadata (PMI) checks ([PMI](https://projects.eclipse.org)),
* Licencing and copyrights ([Scancode](https://github.com/nexB/scancode-toolkit)), and
* Static Code Analysis ([SonarCloud](https://sonarcloud.io)) when available.

Each dataset is composed of:
* Compressed (gzip'd) CSV and JSON files for tool-specific data.
* A full bundle including all above data files related to a project.
* A R Markdown document that analyses the extracted files and provides some hints about how to use them. This document also serves as a validation step to identify empty or inconsistent datasets.

These datasets are published under the [Creative Commons BY-Attribution-Share Alike 4.0 (International) licence](https://creativecommons.org/licenses/by-sa/4.0/). Data is updated weekly, at 2am on Sunday. If you would like to add a project, please [submit an issue](https://gitlab.eclipse.org/bbaldassari2kd/scava-datasets/-/issues).

