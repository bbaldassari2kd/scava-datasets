---
title: List of Eclipse Projects
---

This is the list of all Eclipse projects datasets published for Eclipse Scava. 


## Eclipse APP4MC

* Analysis report: [dataset_report_technology.app4mc.html](../technology.app4mc/dataset_report_technology.app4mc.html)
* PMI home: https://projects.eclipse.org/projects/technology.app4mc
* Downloads:
  - [bugzilla_components.csv.gz](../technology.app4mc/bugzilla_components.csv.gz)
  - [bugzilla_evol.csv.gz](../technology.app4mc/bugzilla_evol.csv.gz)
  - [bugzilla_issues.csv.gz](../technology.app4mc/bugzilla_issues.csv.gz)
  - [bugzilla_issues_open.csv.gz](../technology.app4mc/bugzilla_issues_open.csv.gz)
  - [bugzilla_versions.csv.gz](../technology.app4mc/bugzilla_versions.csv.gz)
  - [eclipse_forums_posts.csv.gz](../technology.app4mc/eclipse_forums_posts.csv.gz)
  - [eclipse_forums_threads.csv.gz](../technology.app4mc/eclipse_forums_threads.csv.gz)
  - [eclipse_pmi_checks.csv.gz](../technology.app4mc/eclipse_pmi_checks.csv.gz)
  - [eclipse_pmi_checks.json.gz](../technology.app4mc/eclipse_pmi_checks.json.gz)
  - [git_commits.csv.gz](../technology.app4mc/git_commits.csv.gz)
  - [git_commits_evol.csv.gz](../technology.app4mc/git_commits_evol.csv.gz)
  - [git_log.txt.gz](../technology.app4mc/git_log.txt.gz)
  - [jenkins_builds.csv.gz](../technology.app4mc/jenkins_builds.csv.gz)
  - [jenkins_jobs.csv.gz](../technology.app4mc/jenkins_jobs.csv.gz)
  - [scancode_authors.csv.gz](../technology.app4mc/scancode_authors.csv.gz)
  - [scancode_copyrights.csv.gz](../technology.app4mc/scancode_copyrights.csv.gz)
  - [scancode_files.csv.gz](../technology.app4mc/scancode_files.csv.gz)
  - [scancode_holders.csv.gz](../technology.app4mc/scancode_holders.csv.gz)
  - [scancode_licences.csv.gz](../technology.app4mc/scancode_licences.csv.gz)
  - [scancode_packages.csv.gz](../technology.app4mc/scancode_packages.csv.gz)
  - [scancode_programming_languages.csv.gz](../technology.app4mc/scancode_programming_languages.csv.gz)
  - [scancode_special_files.csv.gz](../technology.app4mc/scancode_special_files.csv.gz)


## Eclipse Acceleo

* Analysis report: [dataset_report_modeling.m2t.acceleo.html](../modeling.m2t.acceleo/dataset_report_modeling.m2t.acceleo.html)
* PMI home: https://projects.eclipse.org/projects/modeling.m2t.acceleo
* Downloads:
  - [bugzilla_components.csv.gz](../modeling.m2t.acceleo/bugzilla_components.csv.gz)
  - [bugzilla_evol.csv.gz](../modeling.m2t.acceleo/bugzilla_evol.csv.gz)
  - [bugzilla_issues.csv.gz](../modeling.m2t.acceleo/bugzilla_issues.csv.gz)
  - [bugzilla_issues_open.csv.gz](../modeling.m2t.acceleo/bugzilla_issues_open.csv.gz)
  - [bugzilla_versions.csv.gz](../modeling.m2t.acceleo/bugzilla_versions.csv.gz)
  - [eclipse_forums_posts.csv.gz](../modeling.m2t.acceleo/eclipse_forums_posts.csv.gz)
  - [eclipse_forums_threads.csv.gz](../modeling.m2t.acceleo/eclipse_forums_threads.csv.gz)
  - [eclipse_pmi_checks.csv.gz](../modeling.m2t.acceleo/eclipse_pmi_checks.csv.gz)
  - [eclipse_pmi_checks.json.gz](../modeling.m2t.acceleo/eclipse_pmi_checks.json.gz)
  - [git_commits.csv.gz](../modeling.m2t.acceleo/git_commits.csv.gz)
  - [git_commits_evol.csv.gz](../modeling.m2t.acceleo/git_commits_evol.csv.gz)
  - [git_log.txt.gz](../modeling.m2t.acceleo/git_log.txt.gz)
  - [jenkins_builds.csv.gz](../modeling.m2t.acceleo/jenkins_builds.csv.gz)
  - [jenkins_jobs.csv.gz](../modeling.m2t.acceleo/jenkins_jobs.csv.gz)
  - [scancode_authors.csv.gz](../modeling.m2t.acceleo/scancode_authors.csv.gz)
  - [scancode_copyrights.csv.gz](../modeling.m2t.acceleo/scancode_copyrights.csv.gz)
  - [scancode_files.csv.gz](../modeling.m2t.acceleo/scancode_files.csv.gz)
  - [scancode_holders.csv.gz](../modeling.m2t.acceleo/scancode_holders.csv.gz)
  - [scancode_licences.csv.gz](../modeling.m2t.acceleo/scancode_licences.csv.gz)
  - [scancode_packages.csv.gz](../modeling.m2t.acceleo/scancode_packages.csv.gz)
  - [scancode_programming_languages.csv.gz](../modeling.m2t.acceleo/scancode_programming_languages.csv.gz)
  - [scancode_special_files.csv.gz](../modeling.m2t.acceleo/scancode_special_files.csv.gz)


## Eclipse Apogy

* Analysis report: [dataset_report_technology.apogy.html](../technology.apogy/dataset_report_technology.apogy.html)
* PMI home: https://projects.eclipse.org/projects/technology.apogy
* Downloads:
  - [bugzilla_components.csv.gz](../technology.apogy/bugzilla_components.csv.gz)
  - [bugzilla_evol.csv.gz](../technology.apogy/bugzilla_evol.csv.gz)
  - [bugzilla_issues.csv.gz](../technology.apogy/bugzilla_issues.csv.gz)
  - [bugzilla_issues_open.csv.gz](../technology.apogy/bugzilla_issues_open.csv.gz)
  - [bugzilla_versions.csv.gz](../technology.apogy/bugzilla_versions.csv.gz)
  - [eclipse_forums_posts.csv.gz](../technology.apogy/eclipse_forums_posts.csv.gz)
  - [eclipse_forums_threads.csv.gz](../technology.apogy/eclipse_forums_threads.csv.gz)
  - [eclipse_pmi_checks.csv.gz](../technology.apogy/eclipse_pmi_checks.csv.gz)
  - [eclipse_pmi_checks.json.gz](../technology.apogy/eclipse_pmi_checks.json.gz)
  - [git_commits.csv.gz](../technology.apogy/git_commits.csv.gz)
  - [git_commits_evol.csv.gz](../technology.apogy/git_commits_evol.csv.gz)
  - [git_log.txt.gz](../technology.apogy/git_log.txt.gz)
  - [jenkins_builds.csv.gz](../technology.apogy/jenkins_builds.csv.gz)
  - [jenkins_jobs.csv.gz](../technology.apogy/jenkins_jobs.csv.gz)
  - [scancode_authors.csv.gz](../technology.apogy/scancode_authors.csv.gz)
  - [scancode_copyrights.csv.gz](../technology.apogy/scancode_copyrights.csv.gz)
  - [scancode_files.csv.gz](../technology.apogy/scancode_files.csv.gz)
  - [scancode_holders.csv.gz](../technology.apogy/scancode_holders.csv.gz)
  - [scancode_licences.csv.gz](../technology.apogy/scancode_licences.csv.gz)
  - [scancode_packages.csv.gz](../technology.apogy/scancode_packages.csv.gz)
  - [scancode_programming_languages.csv.gz](../technology.apogy/scancode_programming_languages.csv.gz)
  - [scancode_special_files.csv.gz](../technology.apogy/scancode_special_files.csv.gz)


## Eclipse CDT

* Analysis report: [dataset_report_tools.cdt.html](../tools.cdt/dataset_report_tools.cdt.html)
* Project's home: https://www.eclipse.org/cdt
* PMI home: https://projects.eclipse.org/projects/tools.cdt
* Downloads:
  - [bugzilla_components.csv.gz](../tools.cdt/bugzilla_components.csv.gz)
  - [bugzilla_evol.csv.gz](../tools.cdt/bugzilla_evol.csv.gz)
  - [bugzilla_issues.csv.gz](../tools.cdt/bugzilla_issues.csv.gz)
  - [bugzilla_issues_open.csv.gz](../tools.cdt/bugzilla_issues_open.csv.gz)
  - [bugzilla_versions.csv.gz](../tools.cdt/bugzilla_versions.csv.gz)
  - [eclipse_forums_posts.csv.gz](../tools.cdt/eclipse_forums_posts.csv.gz)
  - [eclipse_forums_threads.csv.gz](../tools.cdt/eclipse_forums_threads.csv.gz)
  - [eclipse_pmi_checks.csv.gz](../tools.cdt/eclipse_pmi_checks.csv.gz)
  - [eclipse_pmi_checks.json.gz](../tools.cdt/eclipse_pmi_checks.json.gz)
  - [git_commits.csv.gz](../tools.cdt/git_commits.csv.gz)
  - [git_commits_evol.csv.gz](../tools.cdt/git_commits_evol.csv.gz)
  - [git_log.txt.gz](../tools.cdt/git_log.txt.gz)
  - [jenkins_builds.csv.gz](../tools.cdt/jenkins_builds.csv.gz)
  - [jenkins_jobs.csv.gz](../tools.cdt/jenkins_jobs.csv.gz)
  - [scancode_authors.csv.gz](../tools.cdt/scancode_authors.csv.gz)
  - [scancode_copyrights.csv.gz](../tools.cdt/scancode_copyrights.csv.gz)
  - [scancode_files.csv.gz](../tools.cdt/scancode_files.csv.gz)
  - [scancode_holders.csv.gz](../tools.cdt/scancode_holders.csv.gz)
  - [scancode_licences.csv.gz](../tools.cdt/scancode_licences.csv.gz)
  - [scancode_packages.csv.gz](../tools.cdt/scancode_packages.csv.gz)
  - [scancode_programming_languages.csv.gz](../tools.cdt/scancode_programming_languages.csv.gz)
  - [scancode_special_files.csv.gz](../tools.cdt/scancode_special_files.csv.gz)


## Eclipse EASE

* Analysis report: [dataset_report_technology.ease.html](../technology.ease/dataset_report_technology.ease.html)
* Project's home: https://www.eclipse.org/ease
* PMI home: https://projects.eclipse.org/projects/technology.ease
* Downloads:
  - [bugzilla_components.csv.gz](../technology.ease/bugzilla_components.csv.gz)
  - [bugzilla_evol.csv.gz](../technology.ease/bugzilla_evol.csv.gz)
  - [bugzilla_issues.csv.gz](../technology.ease/bugzilla_issues.csv.gz)
  - [bugzilla_issues_open.csv.gz](../technology.ease/bugzilla_issues_open.csv.gz)
  - [bugzilla_versions.csv.gz](../technology.ease/bugzilla_versions.csv.gz)
  - [eclipse_forums_posts.csv.gz](../technology.ease/eclipse_forums_posts.csv.gz)
  - [eclipse_forums_threads.csv.gz](../technology.ease/eclipse_forums_threads.csv.gz)
  - [eclipse_pmi_checks.csv.gz](../technology.ease/eclipse_pmi_checks.csv.gz)
  - [eclipse_pmi_checks.json.gz](../technology.ease/eclipse_pmi_checks.json.gz)
  - [git_commits.csv.gz](../technology.ease/git_commits.csv.gz)
  - [git_commits_evol.csv.gz](../technology.ease/git_commits_evol.csv.gz)
  - [git_log.txt.gz](../technology.ease/git_log.txt.gz)
  - [jenkins_builds.csv.gz](../technology.ease/jenkins_builds.csv.gz)
  - [jenkins_jobs.csv.gz](../technology.ease/jenkins_jobs.csv.gz)
  - [scancode_authors.csv.gz](../technology.ease/scancode_authors.csv.gz)
  - [scancode_copyrights.csv.gz](../technology.ease/scancode_copyrights.csv.gz)
  - [scancode_files.csv.gz](../technology.ease/scancode_files.csv.gz)
  - [scancode_holders.csv.gz](../technology.ease/scancode_holders.csv.gz)
  - [scancode_licences.csv.gz](../technology.ease/scancode_licences.csv.gz)
  - [scancode_packages.csv.gz](../technology.ease/scancode_packages.csv.gz)
  - [scancode_programming_languages.csv.gz](../technology.ease/scancode_programming_languages.csv.gz)
  - [scancode_special_files.csv.gz](../technology.ease/scancode_special_files.csv.gz)


## Eclipse EGit

* Analysis report: [dataset_report_technology.egit.html](../technology.egit/dataset_report_technology.egit.html)
* Project's home: https://www.eclipse.org/egit
* PMI home: https://projects.eclipse.org/projects/technology.egit
* Downloads:
  - [bugzilla_components.csv.gz](../technology.egit/bugzilla_components.csv.gz)
  - [bugzilla_evol.csv.gz](../technology.egit/bugzilla_evol.csv.gz)
  - [bugzilla_issues.csv.gz](../technology.egit/bugzilla_issues.csv.gz)
  - [bugzilla_issues_open.csv.gz](../technology.egit/bugzilla_issues_open.csv.gz)
  - [bugzilla_versions.csv.gz](../technology.egit/bugzilla_versions.csv.gz)
  - [eclipse_forums_posts.csv.gz](../technology.egit/eclipse_forums_posts.csv.gz)
  - [eclipse_forums_threads.csv.gz](../technology.egit/eclipse_forums_threads.csv.gz)
  - [eclipse_pmi_checks.csv.gz](../technology.egit/eclipse_pmi_checks.csv.gz)
  - [eclipse_pmi_checks.json.gz](../technology.egit/eclipse_pmi_checks.json.gz)
  - [git_commits.csv.gz](../technology.egit/git_commits.csv.gz)
  - [git_commits_evol.csv.gz](../technology.egit/git_commits_evol.csv.gz)
  - [git_log.txt.gz](../technology.egit/git_log.txt.gz)
  - [jenkins_builds.csv.gz](../technology.egit/jenkins_builds.csv.gz)
  - [jenkins_jobs.csv.gz](../technology.egit/jenkins_jobs.csv.gz)
  - [scancode_authors.csv.gz](../technology.egit/scancode_authors.csv.gz)
  - [scancode_copyrights.csv.gz](../technology.egit/scancode_copyrights.csv.gz)
  - [scancode_files.csv.gz](../technology.egit/scancode_files.csv.gz)
  - [scancode_holders.csv.gz](../technology.egit/scancode_holders.csv.gz)
  - [scancode_licences.csv.gz](../technology.egit/scancode_licences.csv.gz)
  - [scancode_packages.csv.gz](../technology.egit/scancode_packages.csv.gz)
  - [scancode_programming_languages.csv.gz](../technology.egit/scancode_programming_languages.csv.gz)
  - [scancode_special_files.csv.gz](../technology.egit/scancode_special_files.csv.gz)


## Eclipse EMF Compare

* Analysis report: [dataset_report_modeling.emfcompare.html](../modeling.emfcompare/dataset_report_modeling.emfcompare.html)
* PMI home: https://projects.eclipse.org/projects/modeling.emfcompare
* Downloads:
  - [bugzilla_components.csv.gz](../modeling.emfcompare/bugzilla_components.csv.gz)
  - [bugzilla_evol.csv.gz](../modeling.emfcompare/bugzilla_evol.csv.gz)
  - [bugzilla_issues.csv.gz](../modeling.emfcompare/bugzilla_issues.csv.gz)
  - [bugzilla_issues_open.csv.gz](../modeling.emfcompare/bugzilla_issues_open.csv.gz)
  - [bugzilla_versions.csv.gz](../modeling.emfcompare/bugzilla_versions.csv.gz)
  - [eclipse_forums_posts.csv.gz](../modeling.emfcompare/eclipse_forums_posts.csv.gz)
  - [eclipse_forums_threads.csv.gz](../modeling.emfcompare/eclipse_forums_threads.csv.gz)
  - [eclipse_pmi_checks.csv.gz](../modeling.emfcompare/eclipse_pmi_checks.csv.gz)
  - [eclipse_pmi_checks.json.gz](../modeling.emfcompare/eclipse_pmi_checks.json.gz)
  - [git_commits.csv.gz](../modeling.emfcompare/git_commits.csv.gz)
  - [git_commits_evol.csv.gz](../modeling.emfcompare/git_commits_evol.csv.gz)
  - [git_log.txt.gz](../modeling.emfcompare/git_log.txt.gz)
  - [jenkins_builds.csv.gz](../modeling.emfcompare/jenkins_builds.csv.gz)
  - [jenkins_jobs.csv.gz](../modeling.emfcompare/jenkins_jobs.csv.gz)
  - [scancode_authors.csv.gz](../modeling.emfcompare/scancode_authors.csv.gz)
  - [scancode_copyrights.csv.gz](../modeling.emfcompare/scancode_copyrights.csv.gz)
  - [scancode_files.csv.gz](../modeling.emfcompare/scancode_files.csv.gz)
  - [scancode_holders.csv.gz](../modeling.emfcompare/scancode_holders.csv.gz)
  - [scancode_licences.csv.gz](../modeling.emfcompare/scancode_licences.csv.gz)
  - [scancode_packages.csv.gz](../modeling.emfcompare/scancode_packages.csv.gz)
  - [scancode_programming_languages.csv.gz](../modeling.emfcompare/scancode_programming_languages.csv.gz)
  - [scancode_special_files.csv.gz](../modeling.emfcompare/scancode_special_files.csv.gz)


## Eclipse EMF Parsley

* Analysis report: [dataset_report_modeling.emf-parsley.html](../modeling.emf-parsley/dataset_report_modeling.emf-parsley.html)
* Project's home: https://www.eclipse.org/emf-parsley
* PMI home: https://projects.eclipse.org/projects/modeling.emf-parsley
* Downloads:
  - [bugzilla_components.csv.gz](../modeling.emf-parsley/bugzilla_components.csv.gz)
  - [bugzilla_evol.csv.gz](../modeling.emf-parsley/bugzilla_evol.csv.gz)
  - [bugzilla_issues.csv.gz](../modeling.emf-parsley/bugzilla_issues.csv.gz)
  - [bugzilla_issues_open.csv.gz](../modeling.emf-parsley/bugzilla_issues_open.csv.gz)
  - [bugzilla_versions.csv.gz](../modeling.emf-parsley/bugzilla_versions.csv.gz)
  - [eclipse_forums_posts.csv.gz](../modeling.emf-parsley/eclipse_forums_posts.csv.gz)
  - [eclipse_forums_threads.csv.gz](../modeling.emf-parsley/eclipse_forums_threads.csv.gz)
  - [eclipse_pmi_checks.csv.gz](../modeling.emf-parsley/eclipse_pmi_checks.csv.gz)
  - [eclipse_pmi_checks.json.gz](../modeling.emf-parsley/eclipse_pmi_checks.json.gz)
  - [git_commits.csv.gz](../modeling.emf-parsley/git_commits.csv.gz)
  - [git_commits_evol.csv.gz](../modeling.emf-parsley/git_commits_evol.csv.gz)
  - [git_log.txt.gz](../modeling.emf-parsley/git_log.txt.gz)
  - [jenkins_builds.csv.gz](../modeling.emf-parsley/jenkins_builds.csv.gz)
  - [jenkins_jobs.csv.gz](../modeling.emf-parsley/jenkins_jobs.csv.gz)
  - [scancode_authors.csv.gz](../modeling.emf-parsley/scancode_authors.csv.gz)
  - [scancode_copyrights.csv.gz](../modeling.emf-parsley/scancode_copyrights.csv.gz)
  - [scancode_files.csv.gz](../modeling.emf-parsley/scancode_files.csv.gz)
  - [scancode_holders.csv.gz](../modeling.emf-parsley/scancode_holders.csv.gz)
  - [scancode_licences.csv.gz](../modeling.emf-parsley/scancode_licences.csv.gz)
  - [scancode_packages.csv.gz](../modeling.emf-parsley/scancode_packages.csv.gz)
  - [scancode_programming_languages.csv.gz](../modeling.emf-parsley/scancode_programming_languages.csv.gz)
  - [scancode_special_files.csv.gz](../modeling.emf-parsley/scancode_special_files.csv.gz)


## Eclipse Epsilon

* Analysis report: [dataset_report_modeling.epsilon.html](../modeling.epsilon/dataset_report_modeling.epsilon.html)
* Project's home: https://www.eclipse.org/epsilon
* PMI home: https://projects.eclipse.org/projects/modeling.epsilon
* Downloads:
  - [bugzilla_components.csv.gz](../modeling.epsilon/bugzilla_components.csv.gz)
  - [bugzilla_evol.csv.gz](../modeling.epsilon/bugzilla_evol.csv.gz)
  - [bugzilla_issues.csv.gz](../modeling.epsilon/bugzilla_issues.csv.gz)
  - [bugzilla_issues_open.csv.gz](../modeling.epsilon/bugzilla_issues_open.csv.gz)
  - [bugzilla_versions.csv.gz](../modeling.epsilon/bugzilla_versions.csv.gz)
  - [eclipse_forums_posts.csv.gz](../modeling.epsilon/eclipse_forums_posts.csv.gz)
  - [eclipse_forums_threads.csv.gz](../modeling.epsilon/eclipse_forums_threads.csv.gz)
  - [eclipse_pmi_checks.csv.gz](../modeling.epsilon/eclipse_pmi_checks.csv.gz)
  - [eclipse_pmi_checks.json.gz](../modeling.epsilon/eclipse_pmi_checks.json.gz)
  - [git_commits.csv.gz](../modeling.epsilon/git_commits.csv.gz)
  - [git_commits_evol.csv.gz](../modeling.epsilon/git_commits_evol.csv.gz)
  - [git_log.txt.gz](../modeling.epsilon/git_log.txt.gz)
  - [jenkins_builds.csv.gz](../modeling.epsilon/jenkins_builds.csv.gz)
  - [jenkins_jobs.csv.gz](../modeling.epsilon/jenkins_jobs.csv.gz)
  - [scancode_authors.csv.gz](../modeling.epsilon/scancode_authors.csv.gz)
  - [scancode_copyrights.csv.gz](../modeling.epsilon/scancode_copyrights.csv.gz)
  - [scancode_files.csv.gz](../modeling.epsilon/scancode_files.csv.gz)
  - [scancode_holders.csv.gz](../modeling.epsilon/scancode_holders.csv.gz)
  - [scancode_licences.csv.gz](../modeling.epsilon/scancode_licences.csv.gz)
  - [scancode_packages.csv.gz](../modeling.epsilon/scancode_packages.csv.gz)
  - [scancode_programming_languages.csv.gz](../modeling.epsilon/scancode_programming_languages.csv.gz)
  - [scancode_special_files.csv.gz](../modeling.epsilon/scancode_special_files.csv.gz)


## Eclipse Gendoc

* Analysis report: [dataset_report_modeling.gendoc.html](../modeling.gendoc/dataset_report_modeling.gendoc.html)
* Project's home: https://www.eclipse.org/gendoc
* PMI home: https://projects.eclipse.org/projects/modeling.gendoc
* Downloads:
  - [bugzilla_components.csv.gz](../modeling.gendoc/bugzilla_components.csv.gz)
  - [bugzilla_evol.csv.gz](../modeling.gendoc/bugzilla_evol.csv.gz)
  - [bugzilla_issues.csv.gz](../modeling.gendoc/bugzilla_issues.csv.gz)
  - [bugzilla_issues_open.csv.gz](../modeling.gendoc/bugzilla_issues_open.csv.gz)
  - [bugzilla_versions.csv.gz](../modeling.gendoc/bugzilla_versions.csv.gz)
  - [eclipse_forums_posts.csv.gz](../modeling.gendoc/eclipse_forums_posts.csv.gz)
  - [eclipse_forums_threads.csv.gz](../modeling.gendoc/eclipse_forums_threads.csv.gz)
  - [eclipse_pmi_checks.csv.gz](../modeling.gendoc/eclipse_pmi_checks.csv.gz)
  - [eclipse_pmi_checks.json.gz](../modeling.gendoc/eclipse_pmi_checks.json.gz)
  - [git_commits.csv.gz](../modeling.gendoc/git_commits.csv.gz)
  - [git_commits_evol.csv.gz](../modeling.gendoc/git_commits_evol.csv.gz)
  - [git_log.txt.gz](../modeling.gendoc/git_log.txt.gz)
  - [jenkins_builds.csv.gz](../modeling.gendoc/jenkins_builds.csv.gz)
  - [jenkins_jobs.csv.gz](../modeling.gendoc/jenkins_jobs.csv.gz)


## Eclipse Glassfish

* Analysis report: [dataset_report_ee4j.glassfish.html](../ee4j.glassfish/dataset_report_ee4j.glassfish.html)
* PMI home: https://projects.eclipse.org/projects/ee4j.glassfish
* Downloads:
  - [eclipse_forums_posts.csv.gz](../ee4j.glassfish/eclipse_forums_posts.csv.gz)
  - [eclipse_forums_threads.csv.gz](../ee4j.glassfish/eclipse_forums_threads.csv.gz)
  - [eclipse_pmi_checks.csv.gz](../ee4j.glassfish/eclipse_pmi_checks.csv.gz)
  - [eclipse_pmi_checks.json.gz](../ee4j.glassfish/eclipse_pmi_checks.json.gz)
  - [git_commits.csv.gz](../ee4j.glassfish/git_commits.csv.gz)
  - [git_commits_evol.csv.gz](../ee4j.glassfish/git_commits_evol.csv.gz)
  - [git_log.txt.gz](../ee4j.glassfish/git_log.txt.gz)
  - [jenkins_builds.csv.gz](../ee4j.glassfish/jenkins_builds.csv.gz)
  - [jenkins_jobs.csv.gz](../ee4j.glassfish/jenkins_jobs.csv.gz)


## Eclipse JGit

* Analysis report: [dataset_report_technology.jgit.html](../technology.jgit/dataset_report_technology.jgit.html)
* PMI home: https://projects.eclipse.org/projects/technology.jgit
* Downloads:
  - [bugzilla_components.csv.gz](../technology.jgit/bugzilla_components.csv.gz)
  - [bugzilla_evol.csv.gz](../technology.jgit/bugzilla_evol.csv.gz)
  - [bugzilla_issues.csv.gz](../technology.jgit/bugzilla_issues.csv.gz)
  - [bugzilla_issues_open.csv.gz](../technology.jgit/bugzilla_issues_open.csv.gz)
  - [bugzilla_versions.csv.gz](../technology.jgit/bugzilla_versions.csv.gz)
  - [eclipse_forums_posts.csv.gz](../technology.jgit/eclipse_forums_posts.csv.gz)
  - [eclipse_forums_threads.csv.gz](../technology.jgit/eclipse_forums_threads.csv.gz)
  - [eclipse_pmi_checks.csv.gz](../technology.jgit/eclipse_pmi_checks.csv.gz)
  - [eclipse_pmi_checks.json.gz](../technology.jgit/eclipse_pmi_checks.json.gz)
  - [git_commits.csv.gz](../technology.jgit/git_commits.csv.gz)
  - [git_commits_evol.csv.gz](../technology.jgit/git_commits_evol.csv.gz)
  - [git_log.txt.gz](../technology.jgit/git_log.txt.gz)
  - [jenkins_builds.csv.gz](../technology.jgit/jenkins_builds.csv.gz)
  - [jenkins_jobs.csv.gz](../technology.jgit/jenkins_jobs.csv.gz)


## Eclipse MDM|BL

* Analysis report: [dataset_report_technology.mdmbl.html](../technology.mdmbl/dataset_report_technology.mdmbl.html)
* PMI home: https://projects.eclipse.org/projects/technology.mdmbl
* Downloads:
  - [bugzilla_components.csv.gz](../technology.mdmbl/bugzilla_components.csv.gz)
  - [bugzilla_evol.csv.gz](../technology.mdmbl/bugzilla_evol.csv.gz)
  - [bugzilla_issues.csv.gz](../technology.mdmbl/bugzilla_issues.csv.gz)
  - [bugzilla_issues_open.csv.gz](../technology.mdmbl/bugzilla_issues_open.csv.gz)
  - [bugzilla_versions.csv.gz](../technology.mdmbl/bugzilla_versions.csv.gz)
  - [eclipse_pmi_checks.csv.gz](../technology.mdmbl/eclipse_pmi_checks.csv.gz)
  - [eclipse_pmi_checks.json.gz](../technology.mdmbl/eclipse_pmi_checks.json.gz)
  - [git_commits.csv.gz](../technology.mdmbl/git_commits.csv.gz)
  - [git_commits_evol.csv.gz](../technology.mdmbl/git_commits_evol.csv.gz)
  - [git_log.txt.gz](../technology.mdmbl/git_log.txt.gz)
  - [jenkins_builds.csv.gz](../technology.mdmbl/jenkins_builds.csv.gz)
  - [jenkins_jobs.csv.gz](../technology.mdmbl/jenkins_jobs.csv.gz)


## Eclipse OCL

* Analysis report: [dataset_report_modeling.mdt.ocl.html](../modeling.mdt.ocl/dataset_report_modeling.mdt.ocl.html)
* PMI home: https://projects.eclipse.org/projects/modeling.mdt.ocl
* Downloads:
  - [bugzilla_components.csv.gz](../modeling.mdt.ocl/bugzilla_components.csv.gz)
  - [bugzilla_evol.csv.gz](../modeling.mdt.ocl/bugzilla_evol.csv.gz)
  - [bugzilla_issues.csv.gz](../modeling.mdt.ocl/bugzilla_issues.csv.gz)
  - [bugzilla_issues_open.csv.gz](../modeling.mdt.ocl/bugzilla_issues_open.csv.gz)
  - [bugzilla_versions.csv.gz](../modeling.mdt.ocl/bugzilla_versions.csv.gz)
  - [eclipse_forums_posts.csv.gz](../modeling.mdt.ocl/eclipse_forums_posts.csv.gz)
  - [eclipse_forums_threads.csv.gz](../modeling.mdt.ocl/eclipse_forums_threads.csv.gz)
  - [eclipse_pmi_checks.csv.gz](../modeling.mdt.ocl/eclipse_pmi_checks.csv.gz)
  - [eclipse_pmi_checks.json.gz](../modeling.mdt.ocl/eclipse_pmi_checks.json.gz)
  - [git_commits.csv.gz](../modeling.mdt.ocl/git_commits.csv.gz)
  - [git_commits_evol.csv.gz](../modeling.mdt.ocl/git_commits_evol.csv.gz)
  - [git_log.txt.gz](../modeling.mdt.ocl/git_log.txt.gz)
  - [jenkins_builds.csv.gz](../modeling.mdt.ocl/jenkins_builds.csv.gz)
  - [jenkins_jobs.csv.gz](../modeling.mdt.ocl/jenkins_jobs.csv.gz)


## Eclipse PDT (PHP Development Tools)

* Analysis report: [dataset_report_tools.pdt.html](../tools.pdt/dataset_report_tools.pdt.html)
* Project's home: https://www.eclipse.org/pdt
* PMI home: https://projects.eclipse.org/projects/tools.pdt
* Downloads:
  - [bugzilla_components.csv.gz](../tools.pdt/bugzilla_components.csv.gz)
  - [bugzilla_evol.csv.gz](../tools.pdt/bugzilla_evol.csv.gz)
  - [bugzilla_issues.csv.gz](../tools.pdt/bugzilla_issues.csv.gz)
  - [bugzilla_issues_open.csv.gz](../tools.pdt/bugzilla_issues_open.csv.gz)
  - [bugzilla_versions.csv.gz](../tools.pdt/bugzilla_versions.csv.gz)
  - [eclipse_forums_posts.csv.gz](../tools.pdt/eclipse_forums_posts.csv.gz)
  - [eclipse_forums_threads.csv.gz](../tools.pdt/eclipse_forums_threads.csv.gz)
  - [eclipse_pmi_checks.csv.gz](../tools.pdt/eclipse_pmi_checks.csv.gz)
  - [eclipse_pmi_checks.json.gz](../tools.pdt/eclipse_pmi_checks.json.gz)
  - [git_commits.csv.gz](../tools.pdt/git_commits.csv.gz)
  - [git_commits_evol.csv.gz](../tools.pdt/git_commits_evol.csv.gz)
  - [git_log.txt.gz](../tools.pdt/git_log.txt.gz)
  - [jenkins_builds.csv.gz](../tools.pdt/jenkins_builds.csv.gz)
  - [jenkins_jobs.csv.gz](../tools.pdt/jenkins_jobs.csv.gz)


## Eclipse Paho

* Analysis report: [dataset_report_technology.paho.html](../technology.paho/dataset_report_technology.paho.html)
* Project's home: https://projects.eclipse.org/proposals/eclipse-glassfish
* PMI home: https://projects.eclipse.org/projects/technology.paho
* Downloads:
  - [bugzilla_components.csv.gz](../technology.paho/bugzilla_components.csv.gz)
  - [bugzilla_evol.csv.gz](../technology.paho/bugzilla_evol.csv.gz)
  - [bugzilla_issues.csv.gz](../technology.paho/bugzilla_issues.csv.gz)
  - [bugzilla_issues_open.csv.gz](../technology.paho/bugzilla_issues_open.csv.gz)
  - [bugzilla_versions.csv.gz](../technology.paho/bugzilla_versions.csv.gz)
  - [eclipse_forums_posts.csv.gz](../technology.paho/eclipse_forums_posts.csv.gz)
  - [eclipse_forums_threads.csv.gz](../technology.paho/eclipse_forums_threads.csv.gz)
  - [eclipse_pmi_checks.csv.gz](../technology.paho/eclipse_pmi_checks.csv.gz)
  - [eclipse_pmi_checks.json.gz](../technology.paho/eclipse_pmi_checks.json.gz)
  - [git_commits.csv.gz](../technology.paho/git_commits.csv.gz)
  - [git_commits_evol.csv.gz](../technology.paho/git_commits_evol.csv.gz)
  - [git_log.txt.gz](../technology.paho/git_log.txt.gz)
  - [jenkins_builds.csv.gz](../technology.paho/jenkins_builds.csv.gz)
  - [jenkins_jobs.csv.gz](../technology.paho/jenkins_jobs.csv.gz)


## Eclipse Papyrus

* Analysis report: [dataset_report_modeling.mdt.papyrus.html](../modeling.mdt.papyrus/dataset_report_modeling.mdt.papyrus.html)
* Project's home: https://www.eclipse.org/papyrus
* PMI home: https://projects.eclipse.org/projects/modeling.mdt.papyrus
* Downloads:
  - [bugzilla_components.csv.gz](../modeling.mdt.papyrus/bugzilla_components.csv.gz)
  - [bugzilla_evol.csv.gz](../modeling.mdt.papyrus/bugzilla_evol.csv.gz)
  - [bugzilla_issues.csv.gz](../modeling.mdt.papyrus/bugzilla_issues.csv.gz)
  - [bugzilla_issues_open.csv.gz](../modeling.mdt.papyrus/bugzilla_issues_open.csv.gz)
  - [bugzilla_versions.csv.gz](../modeling.mdt.papyrus/bugzilla_versions.csv.gz)
  - [eclipse_forums_posts.csv.gz](../modeling.mdt.papyrus/eclipse_forums_posts.csv.gz)
  - [eclipse_forums_threads.csv.gz](../modeling.mdt.papyrus/eclipse_forums_threads.csv.gz)
  - [eclipse_pmi_checks.csv.gz](../modeling.mdt.papyrus/eclipse_pmi_checks.csv.gz)
  - [eclipse_pmi_checks.json.gz](../modeling.mdt.papyrus/eclipse_pmi_checks.json.gz)
  - [git_commits.csv.gz](../modeling.mdt.papyrus/git_commits.csv.gz)
  - [git_commits_evol.csv.gz](../modeling.mdt.papyrus/git_commits_evol.csv.gz)
  - [git_log.txt.gz](../modeling.mdt.papyrus/git_log.txt.gz)
  - [jenkins_builds.csv.gz](../modeling.mdt.papyrus/jenkins_builds.csv.gz)
  - [jenkins_jobs.csv.gz](../modeling.mdt.papyrus/jenkins_jobs.csv.gz)


## Eclipse Process Framework

* Analysis report: [dataset_report_technology.epf.html](../technology.epf/dataset_report_technology.epf.html)
* PMI home: https://projects.eclipse.org/projects/technology.epf
* Downloads:
  - [bugzilla_components.csv.gz](../technology.epf/bugzilla_components.csv.gz)
  - [bugzilla_evol.csv.gz](../technology.epf/bugzilla_evol.csv.gz)
  - [bugzilla_issues.csv.gz](../technology.epf/bugzilla_issues.csv.gz)
  - [bugzilla_issues_open.csv.gz](../technology.epf/bugzilla_issues_open.csv.gz)
  - [bugzilla_versions.csv.gz](../technology.epf/bugzilla_versions.csv.gz)
  - [eclipse_forums_posts.csv.gz](../technology.epf/eclipse_forums_posts.csv.gz)
  - [eclipse_forums_threads.csv.gz](../technology.epf/eclipse_forums_threads.csv.gz)
  - [eclipse_pmi_checks.csv.gz](../technology.epf/eclipse_pmi_checks.csv.gz)
  - [eclipse_pmi_checks.json.gz](../technology.epf/eclipse_pmi_checks.json.gz)
  - [git_commits.csv.gz](../technology.epf/git_commits.csv.gz)
  - [git_commits_evol.csv.gz](../technology.epf/git_commits_evol.csv.gz)
  - [git_log.txt.gz](../technology.epf/git_log.txt.gz)


## Eclipse Scout

* Analysis report: [dataset_report_technology.scout.html](../technology.scout/dataset_report_technology.scout.html)
* Project's home: https://www.eclipse.org/scout
* PMI home: https://projects.eclipse.org/projects/technology.scout
* Downloads:
  - [bugzilla_components.csv.gz](../technology.scout/bugzilla_components.csv.gz)
  - [bugzilla_evol.csv.gz](../technology.scout/bugzilla_evol.csv.gz)
  - [bugzilla_issues.csv.gz](../technology.scout/bugzilla_issues.csv.gz)
  - [bugzilla_issues_open.csv.gz](../technology.scout/bugzilla_issues_open.csv.gz)
  - [bugzilla_versions.csv.gz](../technology.scout/bugzilla_versions.csv.gz)
  - [eclipse_forums_posts.csv.gz](../technology.scout/eclipse_forums_posts.csv.gz)
  - [eclipse_forums_threads.csv.gz](../technology.scout/eclipse_forums_threads.csv.gz)
  - [eclipse_pmi_checks.csv.gz](../technology.scout/eclipse_pmi_checks.csv.gz)
  - [eclipse_pmi_checks.json.gz](../technology.scout/eclipse_pmi_checks.json.gz)
  - [git_commits.csv.gz](../technology.scout/git_commits.csv.gz)
  - [git_commits_evol.csv.gz](../technology.scout/git_commits_evol.csv.gz)
  - [git_log.txt.gz](../technology.scout/git_log.txt.gz)
  - [jenkins_builds.csv.gz](../technology.scout/jenkins_builds.csv.gz)
  - [jenkins_jobs.csv.gz](../technology.scout/jenkins_jobs.csv.gz)
  - [scancode_authors.csv.gz](../technology.scout/scancode_authors.csv.gz)
  - [scancode_copyrights.csv.gz](../technology.scout/scancode_copyrights.csv.gz)
  - [scancode_files.csv.gz](../technology.scout/scancode_files.csv.gz)
  - [scancode_holders.csv.gz](../technology.scout/scancode_holders.csv.gz)
  - [scancode_licences.csv.gz](../technology.scout/scancode_licences.csv.gz)
  - [scancode_packages.csv.gz](../technology.scout/scancode_packages.csv.gz)
  - [scancode_programming_languages.csv.gz](../technology.scout/scancode_programming_languages.csv.gz)
  - [scancode_special_files.csv.gz](../technology.scout/scancode_special_files.csv.gz)


## Eclipse Sirius

* Analysis report: [dataset_report_modeling.sirius.html](../modeling.sirius/dataset_report_modeling.sirius.html)
* Project's home: https://www.eclipse.org/sirius
* PMI home: https://projects.eclipse.org/projects/modeling.sirius
* Downloads:
  - [bugzilla_components.csv.gz](../modeling.sirius/bugzilla_components.csv.gz)
  - [bugzilla_evol.csv.gz](../modeling.sirius/bugzilla_evol.csv.gz)
  - [bugzilla_issues.csv.gz](../modeling.sirius/bugzilla_issues.csv.gz)
  - [bugzilla_issues_open.csv.gz](../modeling.sirius/bugzilla_issues_open.csv.gz)
  - [bugzilla_versions.csv.gz](../modeling.sirius/bugzilla_versions.csv.gz)
  - [eclipse_forums_posts.csv.gz](../modeling.sirius/eclipse_forums_posts.csv.gz)
  - [eclipse_forums_threads.csv.gz](../modeling.sirius/eclipse_forums_threads.csv.gz)
  - [eclipse_pmi_checks.csv.gz](../modeling.sirius/eclipse_pmi_checks.csv.gz)
  - [eclipse_pmi_checks.json.gz](../modeling.sirius/eclipse_pmi_checks.json.gz)
  - [git_commits.csv.gz](../modeling.sirius/git_commits.csv.gz)
  - [git_commits_evol.csv.gz](../modeling.sirius/git_commits_evol.csv.gz)
  - [git_log.txt.gz](../modeling.sirius/git_log.txt.gz)
  - [jenkins_builds.csv.gz](../modeling.sirius/jenkins_builds.csv.gz)
  - [jenkins_jobs.csv.gz](../modeling.sirius/jenkins_jobs.csv.gz)
  - [scancode_authors.csv.gz](../modeling.sirius/scancode_authors.csv.gz)
  - [scancode_copyrights.csv.gz](../modeling.sirius/scancode_copyrights.csv.gz)
  - [scancode_files.csv.gz](../modeling.sirius/scancode_files.csv.gz)
  - [scancode_holders.csv.gz](../modeling.sirius/scancode_holders.csv.gz)
  - [scancode_licences.csv.gz](../modeling.sirius/scancode_licences.csv.gz)
  - [scancode_packages.csv.gz](../modeling.sirius/scancode_packages.csv.gz)
  - [scancode_programming_languages.csv.gz](../modeling.sirius/scancode_programming_languages.csv.gz)
  - [scancode_special_files.csv.gz](../modeling.sirius/scancode_special_files.csv.gz)


## Eclipse Sphinx

* Analysis report: [dataset_report_modeling.sphinx.html](../modeling.sphinx/dataset_report_modeling.sphinx.html)
* Project's home: https://www.eclipse.org/sphinx
* PMI home: https://projects.eclipse.org/projects/modeling.sphinx
* Downloads:
  - [bugzilla_components.csv.gz](../modeling.sphinx/bugzilla_components.csv.gz)
  - [bugzilla_evol.csv.gz](../modeling.sphinx/bugzilla_evol.csv.gz)
  - [bugzilla_issues.csv.gz](../modeling.sphinx/bugzilla_issues.csv.gz)
  - [bugzilla_issues_open.csv.gz](../modeling.sphinx/bugzilla_issues_open.csv.gz)
  - [bugzilla_versions.csv.gz](../modeling.sphinx/bugzilla_versions.csv.gz)
  - [eclipse_forums_posts.csv.gz](../modeling.sphinx/eclipse_forums_posts.csv.gz)
  - [eclipse_forums_threads.csv.gz](../modeling.sphinx/eclipse_forums_threads.csv.gz)
  - [eclipse_pmi_checks.csv.gz](../modeling.sphinx/eclipse_pmi_checks.csv.gz)
  - [eclipse_pmi_checks.json.gz](../modeling.sphinx/eclipse_pmi_checks.json.gz)
  - [git_commits.csv.gz](../modeling.sphinx/git_commits.csv.gz)
  - [git_commits_evol.csv.gz](../modeling.sphinx/git_commits_evol.csv.gz)
  - [git_log.txt.gz](../modeling.sphinx/git_log.txt.gz)
  - [jenkins_builds.csv.gz](../modeling.sphinx/jenkins_builds.csv.gz)
  - [jenkins_jobs.csv.gz](../modeling.sphinx/jenkins_jobs.csv.gz)


## Eclipse Trace Compass

* Analysis report: [dataset_report_tools.tracecompass.html](../tools.tracecompass/dataset_report_tools.tracecompass.html)
* Project's home: https://www.eclipse.org/tracecompass/
* PMI home: https://projects.eclipse.org/projects/tools.tracecompass
* Downloads:
  - [bugzilla_components.csv.gz](../tools.tracecompass/bugzilla_components.csv.gz)
  - [bugzilla_evol.csv.gz](../tools.tracecompass/bugzilla_evol.csv.gz)
  - [bugzilla_issues.csv.gz](../tools.tracecompass/bugzilla_issues.csv.gz)
  - [bugzilla_issues_open.csv.gz](../tools.tracecompass/bugzilla_issues_open.csv.gz)
  - [bugzilla_versions.csv.gz](../tools.tracecompass/bugzilla_versions.csv.gz)
  - [eclipse_forums_posts.csv.gz](../tools.tracecompass/eclipse_forums_posts.csv.gz)
  - [eclipse_forums_threads.csv.gz](../tools.tracecompass/eclipse_forums_threads.csv.gz)
  - [eclipse_pmi_checks.csv.gz](../tools.tracecompass/eclipse_pmi_checks.csv.gz)
  - [eclipse_pmi_checks.json.gz](../tools.tracecompass/eclipse_pmi_checks.json.gz)
  - [git_commits.csv.gz](../tools.tracecompass/git_commits.csv.gz)
  - [git_commits_evol.csv.gz](../tools.tracecompass/git_commits_evol.csv.gz)
  - [git_log.txt.gz](../tools.tracecompass/git_log.txt.gz)
  - [jenkins_builds.csv.gz](../tools.tracecompass/jenkins_builds.csv.gz)
  - [jenkins_jobs.csv.gz](../tools.tracecompass/jenkins_jobs.csv.gz)


## Eclipse Tycho

* Analysis report: [dataset_report_technology.tycho.html](../technology.tycho/dataset_report_technology.tycho.html)
* Project's home: https://www.eclipse.org/tycho/
* PMI home: https://projects.eclipse.org/projects/technology.tycho
* Downloads:
  - [bugzilla_components.csv.gz](../technology.tycho/bugzilla_components.csv.gz)
  - [bugzilla_evol.csv.gz](../technology.tycho/bugzilla_evol.csv.gz)
  - [bugzilla_issues.csv.gz](../technology.tycho/bugzilla_issues.csv.gz)
  - [bugzilla_issues_open.csv.gz](../technology.tycho/bugzilla_issues_open.csv.gz)
  - [bugzilla_versions.csv.gz](../technology.tycho/bugzilla_versions.csv.gz)
  - [eclipse_forums_posts.csv.gz](../technology.tycho/eclipse_forums_posts.csv.gz)
  - [eclipse_forums_threads.csv.gz](../technology.tycho/eclipse_forums_threads.csv.gz)
  - [eclipse_pmi_checks.csv.gz](../technology.tycho/eclipse_pmi_checks.csv.gz)
  - [eclipse_pmi_checks.json.gz](../technology.tycho/eclipse_pmi_checks.json.gz)
  - [git_commits.csv.gz](../technology.tycho/git_commits.csv.gz)
  - [git_commits_evol.csv.gz](../technology.tycho/git_commits_evol.csv.gz)
  - [git_log.txt.gz](../technology.tycho/git_log.txt.gz)
  - [jenkins_builds.csv.gz](../technology.tycho/jenkins_builds.csv.gz)
  - [jenkins_jobs.csv.gz](../technology.tycho/jenkins_jobs.csv.gz)
  - [scancode_authors.csv.gz](../technology.tycho/scancode_authors.csv.gz)
  - [scancode_copyrights.csv.gz](../technology.tycho/scancode_copyrights.csv.gz)
  - [scancode_files.csv.gz](../technology.tycho/scancode_files.csv.gz)
  - [scancode_holders.csv.gz](../technology.tycho/scancode_holders.csv.gz)
  - [scancode_licences.csv.gz](../technology.tycho/scancode_licences.csv.gz)
  - [scancode_packages.csv.gz](../technology.tycho/scancode_packages.csv.gz)
  - [scancode_programming_languages.csv.gz](../technology.tycho/scancode_programming_languages.csv.gz)
  - [scancode_special_files.csv.gz](../technology.tycho/scancode_special_files.csv.gz)
