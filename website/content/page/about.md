---
title: About
menu: sidebar
weight: -10
---

The [Eclipse Foundation](https://eclipse.org) provides individuals and organizations with a commercially focused environment for open source software innovation. It includes git repositories, reviews, issues management, continuous integration, forums and mailing lists among other services. Many well-known and widely used projects are hosted on the forge, including the Eclipse IDE itself, several projects about IoT, modeling, and the new Java working group.

## Crossminer & Scava

[Crossminer](https://crossminer.org) is EU-funded research project that aims at providing tailored recommendations for software practitionners. Its outputs include the [Scava Eclipse project](https://projects.eclipse.org/projects/technology.scava) and a set of public datasets extracted from a selection of representative Eclipse projects.

Crossminer has been terminated in 2019, and since then the datasets are maintained by [Castalia Solutions](https://castalia.solutions) as a service for the Eclipse and Research communities.

Scava is the Eclipse spin-off of Crossminer, a EU-funded research project. More information can be found at the following places:

* The [Eclipse Scava project](https://eclipse.org/scava)
* The official [documentation for Scava](https://scava-docs.readthedocs.io)
* The [documentation repository](https://github.com/crossminer/scava-docs)
* The official [Crossminer web page](https://crossminer.org)
* The [GitHub Crossminer organisation](https://github.com/crossminer)

## Licencing

All datasets are published under the [Creative Commons BY-Attribution-Share Alike 4.0 (International)](https://creativecommons.org/licenses/by-sa/4.0/).

All code is, unless otherwise stated, published under the [Eclipse Public Licence, v2](https://www.eclipse.org/legal/epl-2.0/).


