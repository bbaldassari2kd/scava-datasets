
----

## <a name="sc"></a> ScanCode

### <a name="sc_authors"></a> Authors

**Download**: [`r file_sc_authors`.gz](../`r file_sc_authors`.gz)

```{r sc_a_init}
data <- read.csv(file=file_sc_authors, header=T)
```

File is [``r file_sc_authors``](../`r file_sc_authors`.gz), and has ``r ncol(data)`` columns for ``r nrow(data)`` commits.

<table class="table table-striped">
  <tr><th>Author</th><th>Count</th></tr>
```{r sc_a_table, eval=T, echo=FALSE, message=FALSE, results='asis'}
t <- apply(head(x = data, n = 10), 1,
		   function(x) paste(' <tr><td>', trimws(x[[1]]), '</td><td>', trimws(x[[2]]), '</td></tr>', sep=''))
t <- paste(t, collapse=" ")
cat(t)
```
</table>

```{r sc_a_plot, results='asis'}
suppressPackageStartupMessages(library(googleVis))
options(gvis.plot.tag='chart')

data.sorted <- data[order(data$count, decreasing = T),]

p <- gvisPieChart(data.sorted,
              options = list(
                title=paste("Authors for project ", project_id, " ", sep=""),
                sliceVisibilityThreshold=0, height=280,
                pieHole= 0.4))

print(p, 'chart')
```

<br />

### <a name="sc_copyrights"></a> Copyrights

**Download**: [`r file_sc_copyrights`.gz](../`r file_sc_copyrights`.gz)

```{r sc_c_init}
data <- read.csv(file=file_sc_copyrights, header=T)
```

File is [``r file_sc_copyrights``](../`r file_sc_copyrights`.gz), and has ``r ncol(data)`` columns for ``r nrow(data)`` commits.

<table class="table table-striped">
  <tr><th>Copyrights</th><th>Count</th></tr>
```{r sc_c_table, eval=T, echo=FALSE, message=FALSE, results='asis'}
t <- apply(head(x = data, n = 10), 1,
		   function(x) paste(' <tr><td>', trimws(x[[1]]), '</td><td>', trimws(x[[2]]), '</td></tr>', sep=''))
t <- paste(t, collapse=" ")
cat(t)
```
</table>

```{r sc_c_plot, results='asis'}
suppressPackageStartupMessages(library(googleVis))
options(gvis.plot.tag='chart')

data.sorted <- data[order(data$count, decreasing = T),]

p <- gvisPieChart(data.sorted,
              options = list(
                title=paste("Copyrights for project ", project_id, " ", sep=""),
                sliceVisibilityThreshold=0, height=280,
                pieHole= 0.4))

print(p, 'chart')
```

<br />

### <a name="sc_holders"></a> Holders

**Download**: [`r file_sc_holders`.gz](../`r file_sc_holders`.gz)

```{r sc_h_init}
data <- read.csv(file=file_sc_holders, header=T)
```

File is [``r file_sc_holders``](../`r file_sc_holders`.gz), and has ``r ncol(data)`` columns for ``r nrow(data)`` commits.

<table class="table table-striped">
  <tr><th>Holders</th><th>Count</th></tr>
```{r sc_h_table, eval=T, echo=FALSE, message=FALSE, results='asis'}
t <- apply(head(x = data, n = 10), 1,
		   function(x) paste(' <tr><td>', trimws(x[[1]]), '</td><td>', trimws(x[[2]]), '</td></tr>', sep=''))
t <- paste(t, collapse=" ")
cat(t)
```
</table>

```{r sc_h_plot, results='asis'}
suppressPackageStartupMessages(library(googleVis))
options(gvis.plot.tag='chart')

data.sorted <- data[order(data$count, decreasing = T),]

p <- gvisPieChart(data.sorted,
              options = list(
                title=paste("Holders for project ", project_id, " ", sep=""),
                sliceVisibilityThreshold=0, height=280,
                pieHole= 0.4))

print(p, 'chart')
```

<br />

### <a name="sc_licences"></a> Licences

**Download**: [`r file_sc_licences`.gz](../`r file_sc_licences`.gz)

```{r sc_l_init}
data <- read.csv(file=file_sc_licences, header=T)
```

File is [``r file_sc_licences``](../`r file_sc_licences`.gz), and has ``r ncol(data)`` columns for ``r nrow(data)`` commits.

<table class="table table-striped">
  <tr><th>Licence</th><th>Count</th></tr>
```{r sc_l_table, eval=T, echo=FALSE, message=FALSE, results='asis'}
data <- data[order(data$count, decreasing = T),]
t <- apply(head(x = data, n = 10), 1,
		   function(x) paste(' <tr><td>', trimws(x[[1]]), '</td><td>', trimws(x[[2]]), '</td></tr>', sep=''))
t <- paste(t, collapse=" ")
cat(t)
```
</table>

```{r sc_l_plot, results='asis'}
suppressPackageStartupMessages(library(googleVis))
options(gvis.plot.tag='chart')

p <- gvisPieChart(data,
              options = list(
                title=paste("Licences for project ", project_id, " ", sep=""),
                sliceVisibilityThreshold=0, height=280,
                pieHole= 0.4))

print(p, 'chart')
```

<br />

### <a name="sc_pl"></a> Programming Languages

**Download**: [`r file_sc_pl`.gz](../`r file_sc_pl`.gz)

```{r sc_pl_init}
data <- read.csv(file=file_sc_pl, header=T)
```

File is [``r file_sc_licences``](../`r file_sc_licences`.gz), and has ``r ncol(data)`` columns for ``r nrow(data)`` commits.

<table class="table table-striped">
  <tr><th>Programming Language</th><th>Count</th></tr>
```{r sc_pl_table, eval=T, echo=FALSE, message=FALSE, results='asis'}
data <- data[order(data$count, decreasing = T),]
t <- apply(head(x = data, n = 10), 1,
		   function(x) paste(' <tr><td>', trimws(x[[1]]), '</td><td>', trimws(x[[2]]), '</td></tr>', sep=''))
t <- paste(t, collapse=" ")
cat(t)
```
</table>

```{r sc_pl_plot, results='asis'}
suppressPackageStartupMessages(library(googleVis))
options(gvis.plot.tag='chart')

p <- gvisPieChart(data,
              options = list(
                title=paste("Programming languages for project ", project_id, " ", sep=""),
                sliceVisibilityThreshold=0, height=280,
                pieHole= 0.4))

print(p, 'chart')
```

<br />

### <a name="sc_sf"></a> Special files

**Download**: [`r file_sc_sf`.gz](../`r file_sc_sf`.gz)

```{r sc_sf_init}
data <- read.csv(file=file_sc_sf, header=T)
```

File is [``r file_sc_sf``](../`r file_sc_sf`.gz), and has ``r ncol(data)`` columns for ``r nrow(data)`` commits.

<table class="table table-striped">
  <tr><th>Holders</th><th>Type</th></tr>
```{r sc_sf_table, eval=T, echo=FALSE, message=FALSE, results='asis'}
t <- apply(head(x = data, n = 10), 1,
		   function(x) paste(' <tr><td>', trimws(x[[1]]), '</td><td>', trimws(x[[2]]), '</td></tr>', sep=''))
t <- paste(t, collapse=" ")
cat(t)
```
</table>
